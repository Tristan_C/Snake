# Snake
An old school snake style game written in C using SDL graphics libraries.
It was written and tested in a gnu/linux enviroment, although with some work it should :sweat_smile:
compile and run under windows, assumming [mingw](http://www.mingw.org/) is installed.

# Install instructions for Linux:
## Step 1: Installing SDL
In order to compile snake, we need to ensure that both SDL2 and SDL_TTF are 
installed on your system.
Most distros have these packages in their repositories, but if you cant seem
to find them, you can follow the links below for manual install instructions.
SDL2 can be found at: https://wiki.libsdl.org/Installation
and SDL2_TTF can be found at: https://www.libsdl.org/projects/SDL_ttf/
## Step 2: compiling from source
* First download and unpack the source code, then open up a terminal and `cd` 
into the "source" directory (i.e `cd /home/your_username/Downloads/Snake-master/source`).

* Next type `make` and hit enter to compile the program into an executable 
called 'Snake.out'.

* Now that the program is compiled we can run it with the command `./Snake.out`

# Install Instructions for Windows:
## Step 1: Installing Mingw
Since this program was written under a gnu/Linux toolchain you will first need to 
install Mingw in order to compile under windows. 
Download and install Mingw from: https://sourceforge.net/projects/mingw/files/
__Make sure that you select "gcc" and "make" to be installed when installing mingw!__
__Be sure to add them to your system path as well so you can call them from the command line!__
## Step 2: Compiling from source
* First download and unpack the source code, then open up a terminal and `cd` 
into the "source" directory (i.e `cd C:\Users\your_username\Downloads\Snake\source`).

* Next type `make` and hit enter to compile the program into an executable 
called 'Snake.exe'.  This will also copy some .dll files, these must be in the 
same directory as `snake.exe` in order for it to run.

* The program should now be compiled, and you should be able to run it with the command
`./Snake.exe`




