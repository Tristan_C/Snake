#include <stdio.h>  // printf
#include <stdlib.h> // malloc, realloc, free, rand
#include <time.h>   // time_t
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include "game_objects.h"
#include "game_functions.h"

// Set SCREEN width and height.
const int SCREEN_WIDTH = 675;
const int SCREEN_HEIGHT = 675;

// -------------- Declerations ------------------- //
bool init_SDL(struct game_objs *objs);
void init_game(struct game_objs *objs);
int _get_start_pos(int screen_sze, struct game_objs *objs);
void _set_up_boarder(struct play_area *play_area);
void get_events(struct game_objs *objs);
void _check_direction(struct game_objs *objs);
int move_player(struct game_objs *objs);
void check_fruit(struct game_objs *objs);
void _gen_fruit(struct game_objs *objs);
void draw_play_area(struct game_objs *objs);
void check_colisions(struct game_objs *objs);
void _draw_score(struct game_objs *objs);
void check_game_state(struct game_objs *objs);
void _gameover(struct game_objs *objs);
void inc_score(struct game_objs *objs);
void game_won(struct game_objs *objs);
void _render_txt(char *txt, SDL_Rect txt_loc, int sze, SDL_Surface *screen);
void _free_mem(struct game_objs *objs);
void _close(struct game_objs *objs);
void _resize_snake(struct game_objs *objs);
// -------------- End of Declerations ------------- //

// Initializes SDL2 returning false if any errors are encountered.
bool init_SDL(struct game_objs *objs)
{
    printf("Initializing...\n");

    // Init SDL Video and check for sucsess
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        printf("SDL Could not be initialized! SDL_Error: %s\n", SDL_GetError());
        return false;
    }

    // Create WINDOW at undefined position.
    objs->window = SDL_CreateWindow("Snake",
                                    SDL_WINDOWPOS_UNDEFINED, 
                                    SDL_WINDOWPOS_UNDEFINED,
                                    SCREEN_WIDTH, 
                                    SCREEN_HEIGHT, 
                                    SDL_WINDOW_SHOWN);

    // Check to see if WINDOW creation was sucsessful.
    if (objs->window == NULL)
    {
        printf("WINDOW could not be created! SDL_Error: %s\n", SDL_GetError());
        return false;
    }

    // Init TTF
    if (TTF_Init() == -1)
    {
        printf("TTF_Init: %s\n", TTF_GetError());
        return false;
    }

    objs->screen = SDL_GetWindowSurface(objs->window);
    printf("Sucsessfully Initialized! \n");
    return true;
}

// Initializes game variables and structures, and sets initial game state.
void init_game(struct game_objs *objs)
{
    // Initialize rand.
    time_t t;
    srand((unsigned)time(&t));
    // Allocate mem for structs.
    struct snake *player = (struct snake *)malloc(sizeof(struct snake));
    struct fruit *fruit = (struct fruit *)malloc(sizeof(struct fruit));
    struct events *events = (struct events *)malloc(sizeof(struct events));
    struct play_area *play_area = (struct play_area *)malloc(sizeof(struct play_area));
    // Allocate mem for arrays.
    player->body_segs = (SDL_Rect *)malloc(1225 * sizeof(SDL_Rect));
    play_area->walls = (SDL_Rect *)malloc(4 * sizeof(SDL_Rect));
    // Set play area attributes.
    play_area->wall_thickness = 10;
    play_area->wall_color = SDL_MapRGB(objs->screen->format, 200, 10, 90);
    play_area->bg_color = SDL_MapRGB(objs->screen->format, 0, 0, 0);
    play_area->snake_move_delay = 150;      // in ms
    play_area->blank_space_between_spaces = 5;
    _set_up_boarder(play_area);
    // Set player attributes.
    player->segment_size = 20;
    player->direction = 'N';
    player->length = 1;
    player->needs_resize = true;
    SDL_Rect head;
    head.w = 20;
    head.h = 20;
    player->body_segs[0] = head;
    player->color = SDL_MapRGB(objs->screen->format, 0, 255, 10);
    // set up fruit.
    fruit->color = SDL_MapRGB(objs->screen->format, 200, 100, 40);
    SDL_Rect f_rect;
    f_rect.w = 20;
    f_rect.h = 20;
    fruit->rect = f_rect;
    // Set event attributes.
    events->fruit_eaten = false;
    events->game_over = false;
    events->enter_pressed = false;
    // assemble structs into game_objs struct.
    objs->play_area = play_area;
    objs->events = events;
    objs->fruit = fruit;
    objs->snake = player;
    objs->ticks_old = SDL_GetTicks();
    // Place the player in roughly the middle of the screen.
    objs->snake->body_segs[0].x = _get_start_pos(SCREEN_WIDTH, objs);
    objs->snake->body_segs[0].y = _get_start_pos(SCREEN_HEIGHT, objs);
    // Generate fruit.
    _gen_fruit(objs);
}

/* Displays an animation of a snake pulling the title and instructions up the 
   screen.*/
void title_screen(struct game_objs *objs)
{
    // Set up vars.
    objs->snake->body_segs[0].y = SCREEN_HEIGHT;
    objs->snake->body_segs[0].x = SCREEN_WIDTH * 0.3526;
    objs->fruit->rect.x = -20;
    objs->fruit->rect.y = -20;
    objs->play_area->snake_move_delay = 100;
    objs->play_area->bg_color =
        SDL_MapRGB(objs->screen->format, 255, 255, 255);
    objs->play_area->wall_color =
        SDL_MapRGB(objs->screen->format, 255, 255, 255);
    SDL_Rect title;
    title.w = SCREEN_WIDTH;
    title.h = SCREEN_HEIGHT + objs->snake->segment_size;
    title.x = 0;
    title.y = SCREEN_HEIGHT;
    SDL_Rect title_txt = {SCREEN_WIDTH / 4, title.y + 50, 300, 150};
    SDL_Rect instructions = {SCREEN_WIDTH / 4 - 50, title.y + 250, 300, 150};
    SDL_Rect controls = {SCREEN_WIDTH / 4 + 25, title.y + 300, 300, 150};
    char seq[4] = {'N', 'E', 'N', 'W'};
    int count = 0;
    int dir = 0;
    int moved;

    // main loop.
    while (true)
    {
        // Move the txt an bg up the screen along with the tail of the snake.
        if (title.y > 0)
        {
            title.y = objs->snake->body_segs[objs->snake->length - 1].y + 20;
            title_txt.y = title.y + 50;
            instructions.y = title.y + 250;
            controls.y = title.y + 200;
        }
        // Update the snake's direction
        objs->snake->direction = seq[dir];
        /* Since the snake is only actually moved after 'snake_move_delay' ms,
           we keep track of when a movement actually occurs by placing a + int
           in the moved var. */ 
        moved = -1;
        if (title.y > 0)
        {
            moved = move_player(objs);
        }
        draw_play_area(objs);
        SDL_FillRect(objs->screen, &title,
                     SDL_MapRGB(objs->screen->format, 0, 0, 0));
        _render_txt("SNAKE!", title_txt, 100, objs->screen);
        _render_txt("Press 'ENTER' to begin!", instructions, 40, objs->screen);
        _render_txt("Use 'wsad' to move.", controls, 30, objs->screen);
        SDL_UpdateWindowSurface(objs->window);
        get_events(objs);
        if (objs->events->enter_pressed)
        {
            _free_mem(objs);
            init_game(objs);
            return;
        }
        if (moved > 0)
        {
            if (objs->snake->length < 20)
            {
                _resize_snake(objs);
            }
            count++;
            if (count > 8)
            {
                count = 0;
                dir = (dir + 1) % 4;
            }
        }
    }
}


/* Since the snake must start on a pixel that is 
   (divisable by movement_sze) - wall, in order for the snake to be properly 
   alligned on the board, we find the pixel that satisfies this formula nearest 
   the center of the screen. and return it.*/ 
int _get_start_pos(int screen_sze, struct game_objs *objs)
{
    int wall = objs->play_area->wall_thickness;
    int space = objs->play_area->blank_space_between_spaces;
    int seg_sze = objs->snake->segment_size;
    int movement_sze = seg_sze + space;
    int pos = screen_sze / 2;
    while (true)
    {
        if (pos % movement_sze == 0)
        {
            return pos - wall;
        }
        pos += 1;
    }
}


void _set_up_boarder(struct play_area *play_area)
{
    // Set up north wall.
    SDL_Rect wall;
    wall.x = 0;
    wall.y = 0;
    wall.h = play_area->wall_thickness;
    wall.w = SCREEN_WIDTH;
    play_area->walls[0] = wall;
    // Set up south wall.
    wall.x = 0;
    wall.y = SCREEN_HEIGHT - play_area->wall_thickness;
    wall.h = play_area->wall_thickness;
    wall.w = SCREEN_WIDTH;
    play_area->walls[1] = wall;
    // Set up west wall.
    wall.x = 0;
    wall.y = 0;
    wall.h = SCREEN_HEIGHT;
    wall.w = play_area->wall_thickness;
    play_area->walls[2] = wall;
    // Set up east wall.
    wall.x = SCREEN_WIDTH - play_area->wall_thickness;
    wall.y = 0;
    wall.h = SCREEN_HEIGHT;
    wall.w = play_area->wall_thickness;
    play_area->walls[3] = wall;
}


// Get events from SDL, and update appropriate struct.
void get_events(struct game_objs *objs)
{
    SDL_Event e;

    while (SDL_PollEvent(&e) != 0)
    {
        // Check for exit conditions.
        if ((e.type == SDL_QUIT) || (e.key.keysym.sym == SDLK_ESCAPE))
        {
            _close(objs);
            break;
        }

        // Get directional input.
        if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_w)
        {
            objs->snake->direction = 'N';
            break;
        }

        if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_s)
        {
            objs->snake->direction = 'S';
            break;
        }

        if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_d)
        {
            objs->snake->direction = 'E';
            break;
        }

        if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_a)
        {
            objs->snake->direction = 'W';
            break;
        }

        // Check if enter is pressed.  Used for _gameover and start screens.
        if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_RETURN)
        {
            objs->events->enter_pressed = true;
        }
    }
}

/* Checks to make sure the snakes direction does not conflict with itself, and 
   resets it if need be */
void _check_direction(struct game_objs *objs)
{
    char Direction_old = objs->snake->prev_direction;
    char Direction = objs->snake->direction;
    if (((Direction_old == 'N') && (Direction == 'S')) ||
        ((Direction_old == 'S') && (Direction == 'N')) ||
        ((Direction_old == 'E') && (Direction == 'W')) ||
        ((Direction_old == 'W') && (Direction == 'E')))
    {
        objs->snake->direction = Direction_old;
    }
}

/* Checks to see if snake_move_delay ms have passed, and if so, moves player by 
 * seg_sze + blank_space_between_spaces, in the appropriate direcion.
 * 
 * snake_move_delay directly controls how fast the snake moves across the 
 * screen, with a shorter delay resulting in faster movement. 
 * 
 * returns -1 if snake was not actually moved (due to not enough time having 
 * passed since last move) and +1 otherwise.
 */
int move_player(struct game_objs *objs)
{
    Uint32 cur_ticks = SDL_GetTicks();
    if (cur_ticks - objs->ticks_old < objs->play_area->snake_move_delay)
    {
        return -1;
    }
    objs->ticks_old = cur_ticks;

    _check_direction(objs);
    SDL_Rect *arr = objs->snake->body_segs;
    int arr_sze = objs->snake->length;
    int seg_sze = objs->snake->segment_size;
    int blank = objs->play_area->blank_space_between_spaces;
    objs->snake->prev_direction = objs->snake->direction;

    for (int seg = arr_sze - 1; seg >= 0; seg--)
    {
        // Move the head in the appropriate direction.
        if (seg == 0)
        {
            switch (objs->snake->direction)
            {
            case 'N':
                arr[seg].y -= (seg_sze + blank);
                break;
            case 'S':
                arr[seg].y += (seg_sze + blank);
                break;
            case 'E':
                arr[seg].x += (seg_sze + blank);
                break;
            case 'W':
                arr[seg].x -= (seg_sze + blank);
                break;
            }
        }
        // Move each body seg to the old position of the previous seg.
        else
        {
            arr[seg].x = arr[seg - 1].x;
            arr[seg].y = arr[seg - 1].y;
        }
    }
    return 1;
}


// Generates random coordinates for fruit.
void _gen_fruit(struct game_objs *objs)
{
    int wall = objs->play_area->wall_thickness;
    int space = objs->play_area->blank_space_between_spaces;
    int seg_sze = objs->snake->segment_size;
    int movement = seg_sze + space;
    int x;
    int y;

    /* Both x and y coords are generated by choosing a random number and 
     * checking if it is on the screen, and divisible by 
     * segment_size + blank_space_between_spaces, if so,
     * subtract wall_thickness from it. (this is done to ensure snake and
     * fruit line up.) 
     */

    // Gen x coord.
    while (true)
    {   
        x = rand() % ((SCREEN_WIDTH - wall) + 1 - wall) + wall;
        if (x % movement == 0 && x >= (wall + space) &&
            x <= SCREEN_WIDTH - wall - space - seg_sze)
        {
            x -= 10;
            break;
        }
    }

    // Gen y coord.
    while (true)
    {
        y = rand() % ((SCREEN_HEIGHT - wall) + 1 - wall) + wall;
        if (y % movement == 0 && y >= (wall + space) &&
            y <= SCREEN_HEIGHT - wall - space - seg_sze)
        {
            y -= 10;
            break;
        }
    }
    // Update fruit's struct.
    objs->fruit->rect.x = x;
    objs->fruit->rect.y = y;
}


void draw_play_area(struct game_objs *objs)
{
    // Draw background.
    SDL_FillRect(objs->screen, NULL, objs->play_area->bg_color);
    // Draw boarder.
    SDL_FillRects(objs->screen, objs->play_area->walls, 4,
                  objs->play_area->wall_color);
    // Draw score.
    _draw_score(objs);
    // Draw Fruit.
    SDL_FillRect(objs->screen, &objs->fruit->rect, objs->fruit->color);
    // Draw Snake.
    SDL_FillRects(objs->screen, objs->snake->body_segs, objs->snake->length,
                  objs->snake->color);
}


void _draw_score(struct game_objs *objs)
{
    char *txt = (char *)calloc(20, sizeof(char));
    sprintf(txt, "Score = %i", objs->snake->length - 1);
    SDL_Rect txt_loc;
    txt_loc.x = 15;
    txt_loc.y = 15;
    txt_loc.h = 30;
    txt_loc.w = 60;
    _render_txt(txt, txt_loc, 20, objs->screen);
    free(txt);
}


void check_colisions(struct game_objs *objs)
{
    // Check for collisions against walls.
    for (int i = 0; i < 4; i++)
    {
        if (SDL_HasIntersection(&objs->snake->body_segs[0],
                                &objs->play_area->walls[i]) == SDL_TRUE)
        {
            objs->events->game_over = true;
            return;
        }
    }

    // Check collisions between snake and itself.
    for (int s = 1; s < objs->snake->length; s++)
    {
        if (SDL_HasIntersection(&objs->snake->body_segs[0],
                                &objs->snake->body_segs[s]) == SDL_TRUE)
        {
            objs->events->game_over = true;
            return;
        }
    }

    // check for colision with fruit.
    if (SDL_HasIntersection(&objs->snake->body_segs[0],
                            &objs->fruit->rect) == SDL_TRUE)
    {
        objs->events->fruit_eaten = true;
    }
}


// Checks the events struct and calls appropriate function.
void check_game_state(struct game_objs *objs)
{
    if (objs->events->game_over)
    {
        _gameover(objs);
    }
    if (objs->events->fruit_eaten)
    {
        _gen_fruit(objs);
        objs->events->fruit_eaten = false;
        _resize_snake(objs);
    }
}


// Displays a gameover screen.
void _gameover(struct game_objs *objs)
{
    SDL_Rect rect;
    rect.h = SCREEN_WIDTH / 2;
    rect.w = SCREEN_WIDTH / 2;
    rect.x = rect.w / 2 + 25;
    rect.y = rect.h / 2 + 25;
    SDL_Rect rect_2 = rect;
    rect_2.x -= 30;
    rect_2.y += 75;
    while (true)
    {
        _render_txt("GAME OVER", rect, 50, objs->screen);
        _render_txt("Press 'ENTER' to play again.", rect_2, 30, objs->screen);
        _draw_score(objs);
        SDL_UpdateWindowSurface(objs->window);
        get_events(objs);
        if (objs->events->enter_pressed)
        {
            _free_mem(objs);
            init_game(objs);
            break;
        }
    }
}


void _resize_snake(struct game_objs *objs)
{
    SDL_Rect rect;
    rect.w = objs->snake->segment_size;
    rect.h = objs->snake->segment_size;
    // Set x and y off the screen to avoid flashing squares when size increased.
    rect.x = -20;
    rect.y = SCREEN_HEIGHT;
    objs->snake->length += 1;
    objs->snake->body_segs[objs->snake->length - 1] = rect;
}


void _render_txt(char *txt, SDL_Rect txt_loc, int sze, SDL_Surface *screen)
{
    TTF_Font *Sans = TTF_OpenFont("/usr/share/fonts/gnu-free/FreeSans.ttf", 
                                  sze);
    if (Sans == NULL){
        Sans = TTF_OpenFont("/Windows/Fonts/LSANS.TTF", sze);
    }
    if (Sans == NULL){
        printf("ERROR opening font.\n");
        exit(-1);
    }

    SDL_Color foregroundColor = {255, 255, 255};
    SDL_Color backgroundColor = {0, 0, 0};

    SDL_Surface *textSurface = TTF_RenderText_Solid(Sans, txt, foregroundColor);

    SDL_BlitSurface(textSurface, NULL, screen, &txt_loc);

    SDL_FreeSurface(textSurface);

    TTF_CloseFont(Sans);
}

// Frees allocated mem.
void _free_mem(struct game_objs *objs)
{
    free(objs->snake->body_segs);
    free(objs->play_area->walls);
    free(objs->snake);
    free(objs->fruit);
    free(objs->play_area);
    free(objs->events);
}

// close window, free mem, quit SDL, and exit gracefully.
void _close(struct game_objs *objs)
{
    printf("Exit conditions met. Quitting SDL.\n");
    SDL_FreeSurface(objs->screen);
    SDL_DestroyWindow(objs->window);
    TTF_Quit();
    SDL_Quit();
    _free_mem(objs);
    free(objs);
    exit(0);
}
