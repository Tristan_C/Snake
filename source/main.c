#include <stdio.h>  // printf
#include <stdlib.h> // malloc, realloc, free
#include <SDL2/SDL.h>
#include "game_objects.h"
#include "game_functions.h"


int main(int argc, char *argv[])
{
    /* Allocate mem for our game_objs struct, which is used to contain nearly 
       all important info about the game. */
    struct game_objs *objs =
        (struct game_objs *)malloc(sizeof(struct game_objs));
    if (! init_SDL(objs)){
        printf("An error was encountered while initializing SDL. Exiting.\n");
        free(objs);
        exit(-1);
    }
    init_game(objs);
    title_screen(objs);
    // Game loop
    while (true)
    {
        get_events(objs);
        move_player(objs);
        check_colisions(objs);
        check_game_state(objs);
        draw_play_area(objs);
        SDL_UpdateWindowSurface(objs->window);
    }
}
