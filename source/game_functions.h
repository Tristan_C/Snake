#ifndef _game_functions_h
#define _game_functions_h

bool init_SDL(struct game_objs *objs);
void init_game(struct game_objs *objs);
void get_events(struct game_objs *objs);
int move_player(struct game_objs *objs);
void draw_play_area(struct game_objs *objs);
void check_colisions(struct game_objs *objs);
void check_game_state(struct game_objs *objs);
void title_screen(struct game_objs *objs);

#endif
