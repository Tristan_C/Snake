#ifndef _game_objects_h
#define _game_objects_h

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

// Define boolean type.
typedef enum{false, true} bool;


struct snake
{
    int length;
    int segment_size;
    char direction;
    char prev_direction;
    SDL_Rect *body_segs;
    bool needs_resize;
    Uint32 color;
};


struct fruit
{
    SDL_Rect rect;
    Uint32 color;
};


struct play_area
{
    SDL_Rect *walls;
    int wall_thickness;
    Uint32 wall_color;
    Uint32 bg_color;
    int snake_move_delay;
    int blank_space_between_spaces;
};


struct events
{
    bool fruit_eaten;
    bool game_over;
    bool enter_pressed;
};


struct game_objs
{
    SDL_Window *window;
    SDL_Surface *screen;
    Uint32 ticks_old;
    struct snake *snake;
    struct fruit *fruit;
    struct play_area *play_area;
    struct events *events;
};

#endif
